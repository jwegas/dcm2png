import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="dcm2png",
    version="2.3.4",
    author="ZEA",
    description="Convertor from DICOM to PNG.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jwegas/dcm2png",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    python_requires='>=3.7',
    install_requires=[
        "numpy>=1.18.1",
        "pydicom==2.1.2",
        "joblib==0.17.0",
        "MedPy==0.4.0",
        "opencv-python==4.4.0.46",
        "python-gdcm==3.0.8.1",
        "tqdm>=4.54.0",
        "requests==2.24.0"
    ]
)
