#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import logging
from typing import Tuple

import pydicom
from medpy.io import load as medpy_load
import numpy as np
import cv2
import requests


ZERO_VALUE = 10
EMPTY_VALUE = 255


#  setup logger
logger = logging.getLogger()


class DCM2PNG():

    def __init__(self, path=None, dcm=None, y_min=0, y_max=255,
                 wc=None, ww=None, new_size=None,
                 use_lut: bool = False):
        """Convert DICOM file to PNG

        This class can be used to cenvert DICOM file to RGB(A) png with two
        options:
        * clean grey RGB image where channels equal to each oather;
        * CMAI image, where R is greyscale image, G and B are parts of
            The Hounsfield unit (HU) scale. And A contains flag what tipe of
            Photometric Interpretation DICOM has.

        Either `path` or `dcm` can be passed.

        Parameters:
        -----------
            path - str;
                path to raw DICOM file
            dcm - pydicom.FileDataset object
        """

        #  base attributes
        self._path = path

         #  load raw dicom file with `pydicom`
        self.dcm = self._load_raw_dicom(path) if dcm is None else dcm

        #  parse DICOM
        #  1. extract info about photometric (need to invert)
        self.pi = self._get_photometric_interpretation()
        self._ignore_pi = False

        #  2. extract raw pixel array and rescale it
        self.pixel_array = self._get_pixel_array()

        #  3. extract window center and window width
        self.use_lut = use_lut
        if wc is None or ww is None:
            self.wc, self.ww = self._get_window_params()
        else:
            self.wc, self.ww = wc, ww

        #  4. internal sevice - vectorize some functions
        self._put_value_into_interval =\
            np.vectorize(self._put_value_into_interval)
        self._get_green = np.vectorize(self._get_green)
        self._get_blue = np.vectorize(self._get_blue)

        #  5. get study uid
        self.study_uid = self.dcm.get('StudyInstanceUID')
        if self.study_uid:
            self.study_uid = self.study_uid.replace('.', '_')

        #  6. get series name
        self.series_uid = self.dcm.get('SeriesInstanceUID')
        if self.series_uid:
            self.series_uid = self.series_uid.replace('.', '_')

        #  7. get number of slice / instance
        self.instance_number = str(self.dcm.get('InstanceNumber', 0))

        #  8. get thickness of slice
        self.slice_thickness = float(self.dcm.get('SliceThickness', 0))

    def _get_window_params(self):
        """Extract window center and window width from meta"""

        wc = None
        ww = None

        wc = self.dcm.get('WindowCenter')
        if wc:
            if isinstance(wc, pydicom.valuerep.DSfloat):
                wc = [wc]
            elif isinstance(wc, str):
                wc = [wc.replace(',', '.')]
            wc = float(wc[0])
        else:
            wc = 0.0

        ww = self.dcm.get('WindowWidth')
        if ww:
            if isinstance(ww, pydicom.valuerep.DSfloat):
                ww = [ww]
            elif isinstance(ww, str):
                ww = [ww.replace(',', '.')]
            ww = float(ww[0])
        else:
            ww = 0.0

        return wc, ww

    def _get_photometric_interpretation(self):
        pi = {'MONOCHROME1': 1, 'MONOCHROME2': 2}
        return pi[self.dcm.get('PhotometricInterpretation', 'MONOCHROME2')]

    def _get_pixel_array(self):
        """Get rescaled pixels from DICOM"""

        #  get raw pixels
        try:
            pixels = self.dcm.pixel_array
        except ValueError:
            logger.debug("Error during getting pixels with `pydiom`. Try `medpy`")
            pixels, _ = medpy_load(self._path)
            pixels = np.squeeze(pixels)
            pixels = cv2.rotate(pixels, cv2.ROTATE_90_CLOCKWISE)
            pixels = cv2.flip(pixels, 1)
            self._ignore_pi = True
        except Exception:
            logger.error("Error during getting pixels with `pydiom` and `medpy`")
            return

        #  rescale
        rs = float(self.dcm.get('RescaleSlope', 1))
        ri = float(self.dcm.get('RescaleIntercept', 0))
        pixels = (np.float64(pixels) * rs + ri).astype(pixels.dtype)
        return pixels

    def _load_raw_dicom(self, path):
        """Load RAW DICOM from local path or url"""

        logger.info(f"Load raw DICOM from: {path}")
        if path.startswith("http"):
            response = requests.get(path)
            dcm = pydicom.dcmread(pydicom.filebase.DicomBytesIO(
                response.content))
        else:
            dcm = pydicom.read_file(path)
        return dcm

    def _get_resize_coef(self, size, img):
        resize_coef = 1. * int(size) / max(img.shape)
        return resize_coef

    def _resize(self, size, img):
        img = img.copy()
        resize_coef = self._get_resize_coef(size, img)
        img = cv2.resize(
            img,
            (int(round(img.shape[1] * resize_coef)),
             int(round(img.shape[0] * resize_coef))))
        return img

    def _put_value_into_interval(self, x):
        """Put given value of HU to required interval.
        """
        if x < 0:
            return x + 65536
        else:
            return x

    def _get_green(self, x):
        """Get 1st part of 16-bit variant of HU.
        """
        res = hex(np.uint16(x))
        res = res[2:]
        res = res.zfill(4)
        res = int(res[:2], 16)
        return res

    def _get_blue(self, x):
        """Get 2nd part of 16-bit variant of HU.
        """
        res = hex(np.uint16(x))
        res = res[2:]
        res = res.zfill(4)
        res = int(res[2:], 16)
        return res

    def get_cmai(self, resize=None):
        if hasattr(self, '_img_cmai'):
            return self._img_cmai

        #  get R - simple grayscale
        R = self.get_gray(resize)
        R = np.expand_dims(R, 2)

        #  create Alpha channel
        A = np.ones(shape=R.shape[:2], dtype=np.uint8)
        A *= EMPTY_VALUE

        #  add info about Photometric Interpreation in Alpha channel
        A[0, 0] = self.pi

        #  add information about spacing
        pixel_spacing_raw = self.dcm.get('PixelSpacing')
        resize_coef = self._get_resize_coef(resize, self.pixel_array) if resize else 1.0
        if pixel_spacing_raw is not None:
            pixel_spacing_raw = [
                float(x) / resize_coef for x in pixel_spacing_raw]
            pixel_spacing = []
            for ps in pixel_spacing_raw:
                pixel_spacing += str(ps).split('.')

            for i, ps in enumerate(pixel_spacing):
                for j in range(min(len(ps), A.shape[1])):
                    A[i + 1, j] = (ps[j]).replace('0', str(ZERO_VALUE))

        #  add information about instance number
        for j, x in enumerate(str(self.instance_number)):
            A[5, j] = x.replace('0', str(ZERO_VALUE))

        #  add iformation about slice thickness
        slice_thickness_str = str(round(self.slice_thickness, 4))
        for i, number_part in enumerate(slice_thickness_str.split('.')):
            for j, x in enumerate(number_part):
                A[i + 6, j] = x.replace('0', str(ZERO_VALUE))

        #  add special stamp/sign for our converted images
        #  [4, 3, 13] = dcm - number of letters in alphabet
        A[-1, :3] = [4, 3, 13]
        A[-1, -3:] = [4, 3, 13]

        A = np.expand_dims(A, 2)

        #  get blue and green channels
        pixel_array = self.pixel_array.copy()
        if resize:
            pixel_array = self._resize(resize, pixel_array)
        GB = self._put_value_into_interval(pixel_array).astype(np.uint16)
        if resize:
            GB = self._resize(resize, GB)
        G = self._get_green(GB).astype(np.uint8)
        B = self._get_blue(GB).astype(np.uint8)

        #  add 3rd dimension
        G = np.expand_dims(G, 2)
        B = np.expand_dims(B, 2)

        img = np.concatenate([R, G, B, A], axis=2)

        self._img_cmai = img.copy()

        return self._img_cmai

    def get_rgb(self, resize=None):
        if hasattr(self, '_img_rgb'):
            return self._img_rgb

        img = self.get_gray(resize)
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
        self._img_rgb = img.copy()

        return self._img_rgb

    def get_gray(self, resize=None):
        if hasattr(self, '_img_gray'):
            return self._img_gray

        #  get copy of pixel array
        if self.pixel_array is None:
            return None

        img = self.pixel_array.copy()

        #  noremalize according to window parameters
        if self.use_lut:
            img = pydicom.pixel_data_handlers.apply_voi_lut(img, self.dcm)

        elif all([self.wc, self.ww]):

            l_and = np.logical_and
            l_not = np.logical_not

            y_min_cond =\
                self.pixel_array <= (self.wc - 0.5 - (self.ww - 1) / 2)

            y_max_cond =\
                self.pixel_array > (self.wc - 0.5 + (self.ww - 1) / 2)

            other_cond = l_and(l_not(y_min_cond), l_not(y_max_cond))

            img[y_min_cond] = 0
            img[y_max_cond] = 255

            img[other_cond] =\
                ((img[other_cond] - self.wc - 0.5) / (self.ww - 1) + 0.5) * 255

        #  invert image if required
        is_inverted = self.pi == 1 and not self._ignore_pi
        if is_inverted:
            img = np.invert(img.astype(np.uint16))

        #  normalize to interval [0, 255]
        img = cv2.normalize(src=img, dst=None, alpha=0, beta=255,
                            norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

        #  resize image if required
        if resize:
            img = self._resize(resize, img)

        self._img_gray = img.copy()

        return self._img_gray

    def _check_pixels_exist(self, func):
        def wrapper(self, *args, **kwargs):
            if self.pixel_array is None:
                return None
            return func(*args, **kwargs)
        return wrapper

    def save_png(self, output_path, mode='gray', resize=None):
        mode = str(mode).lower()

        if self.dcm is None:
            logger.error(f"`dcm` attribute is not loaded.")
            return False
        elif mode == 'gray':
            img = self.get_gray(resize)
        elif mode == 'cmai':
            img = self.get_cmai(resize)
            img = cv2.cvtColor(img, cv2.COLOR_RGBA2BGRA)
        elif mode == 'rgb':
            img = self.get_rgb(resize)
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        else:
            logger.error("Error during saving png!")
            return False

        is_saved = cv2.imwrite(output_path, img)
        if not is_saved:
            logger.error(f"Image could not be saved to {output_path}")

        return is_saved

    def apply_overlay(self, image: np.ndarray) -> Tuple[np.ndarray, bool]:

        #  try to extract overlay layer
        try:
            overlay = self.dcm.overlay_array(0x6000)
            overlay_y1, overlay_x1 = self.dcm['6000', '0050'].value
            overlay_y1 = overlay_y1 - 1
            overlay_x1 = overlay_x1 - 1

        except:
            overlay = None

        #  if fail occured during overlay exctraction then return source image
        if overlay is None:
            return image, False


        #  get bottom right point
        overlay_x2 = overlay.shape[1] + overlay_x1
        overlay_y2 = overlay.shape[0] + overlay_y1

        #  crop overlay if it is outside image
        overlay_y_margin = min(overlay.shape[0], image.shape[0] - overlay_y1)
        overlay_x_margin = min(overlay.shape[1], image.shape[1] - overlay_x1)
        overlay = overlay[:overlay_y_margin, :overlay_x_margin]

        #  update bottom right point
        overlay_x2 = overlay.shape[1] + overlay_x1
        overlay_y2 = overlay.shape[0] + overlay_y1

        overlayed_image = image.copy()
        overlayed_image[overlay_y1:overlay_y2,
                        overlay_x1:overlay_x2][overlay == 1] = 255

        return overlayed_image, True
