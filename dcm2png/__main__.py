#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
import logging
import argparse
import os
import joblib
from tqdm import tqdm
from dcm2png import DCM2PNG


#  get parameters from command line
parser = argparse.ArgumentParser()

parser.add_argument(
    '--src-path',
    required=True,
    help='Source path to DICOM file or dir with DICOM file')

parser.add_argument(
    '--dst-path',
    help="""Output path to save converted DICOM files.
         If parameters is not set then converted file or dir with converted
         files will be created near to source files""")

parser.add_argument(
    '--mode',
    default='gray',
    help="""The way to convert DICOM - `gray` (just greyscale image), `rgb` (3-channel grayscale)
            or `cmai` - image with HU values in G and B channels and
            additional information in A channel""")

parser.add_argument(
    '--new-size', type=int,
    help="""Resize converted images to new size. Computed by bigger side.
            For example --new-size=1500"""
)

parser.add_argument(
    '--use-siuid',
    default=False, action='store_true',
    help="""Use Study Instance UID to group DICOM files.""")

parser.add_argument(
    '--postfix',
    default=False, action='store_true',
    help='Add postfix to filenames _{i}, where `i` is slice order in DICOM'
)

parser.add_argument(
    '--workers',
    default=1, type=int,
    help='Add postfix to filenames _{i}, where `i` is slice order in DICOM'
)

parser.add_argument(
    '--force',
    default=False, action='store_true',
    help='Replace files if they exist.'
)

parser.add_argument(
    '--log',
    help='Path to save log.'
)

parser.add_argument(
    '--silent',
    default=False, action='store_true',
    help='Hide printed log.'
)


args = parser.parse_args()

src_path_root = args.src_path
dst_path_root = args.dst_path

# logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

if args.log:
    log_handler = logging.FileHandler(args.log, mode='a',)
    logger.addHandler(log_handler)
if args.silent:
    logger.propagate = False

logger.info('Converter works with mode: {}...'.format(args.mode))
logger.info('Source path: {}'.format(src_path_root))
logger.info('Output path: {}'.format(dst_path_root))


def process_dicom(root_dir, file_dir, file_name, dst_dir,
                   new_size=None, force=False,
                   use_siuid=False, postfix=None, mode='gray'):
    src_path = os.path.join(file_dir, file_name)
    try:
        dcm = DCM2PNG(src_path)
        dst_path = file_dir.replace(root_dir, dst_dir)
        if not os.path.exists(dst_path):
            os.makedirs(dst_path, exist_ok=True)
        # if dcm.series_uid:
        #     output_filename = dcm.series_uid
        if dcm.study_uid and use_siuid:
            output_filename = dcm.study_uid
        else:
            output_filename = os.path.splitext(file_name)[0]
        if postfix:
            output_filename = '_'.join([output_filename, dcm.instance_number])
        dst_path = os.path.join(
            dst_path, output_filename + '.png')
        if not os.path.exists(dst_path) or force:
            dcm.save_png(dst_path, mode=mode, resize=new_size)

    except Exception:
        logger.info('{}\nFile is not DICOM or broken.'
            .format(src_path))


#  main process
if os.path.isfile(src_path_root):
    logger.info('One File Mode')
    if dst_path_root is None:
        dst_path_root = src_path_root + '.png'
    if dst_path_root[-1] == '/':
        dst_path_root = os.path.join(
            dst_path_root,
            os.path.splitext(os.path.split(src_path_root)[1])[0] + '.png')

    dcm = DCM2PNG(src_path_root)
    dcm.save_png(dst_path_root, mode=args.mode)

else:
    logger.info('Directory Mode')
    if dst_path_root is None:
        dst_path_root = src_path_root + '_png/'

    if not os.path.exists(dst_path_root):
        os.mkdir(dst_path_root)

    total_files = 0
    dcm_paths = []
    for root, dirs, files in os.walk(src_path_root):
        total_files += len(files)
        for f in files:
            dcm_paths.append((root, f))

    _ = joblib.Parallel(n_jobs=args.workers)(joblib.delayed(process_dicom)(
        src_path_root, path[0], path[1], dst_path_root, args.new_size, args.force, args.use_siuid, args.postfix, args.mode
        ) for path in tqdm(dcm_paths))

logger.info('Converting is finished! Have a nice day!')
